(function ($) {
  Drupal.behaviors.rulesCheckNull = {
    attach: function (context, settings) {
      $('[name="negate"]').change(function () {
        if ($(this).is(':checked')) {
          alert(settings.rulesCheckNullText);
        }
      });
    }
  };
}(jQuery));
