<?php

/**
 * Admin settings form.
 */
function rules_check_null_admin_settings_form($form, &$form_state) {
  $form['rules_check_null_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Alert text'),
    '#description' => t("This text is shown as JavaScript alert when someone checks <em>Negate</em> checkbox on new <em>Data Comparison</em> Rules condition."),
    '#default_value' => variable_get('rules_check_null_text', 'Do not forget to check against NULL value!'),
  );
  return system_settings_form($form);
}
